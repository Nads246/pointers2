#include <iostream>

using namespace std;


int main()
{
  int x = 25, y = 2 * x;    
  auto a = &x, b = a;
  auto c = *a, d = 2 * *b;


  cout<<"The value of x is "<<x<<" and the address of x is "<<&x<<endl;
  cout<<"The value of y is "<<y<<" and the address of y is "<<&y<<endl;
  cout<<"The value of a is "<<x<<" and the address of a is "<<a<<endl;
  cout<<"The value of b is "<<*a<<" and the address of b is "<<b<<endl;
  cout<<"The value of c is "<<c<<" and the address of c is "<<a<<endl;
  cout<<"The value of d is "<<d<<" and the address of d is "<<&d<<endl;
}
